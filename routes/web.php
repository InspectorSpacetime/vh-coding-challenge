<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which 
| contains the "web" middleware group. Now create something great!
|
*/

// Get
Route::get('/', 'QuestionsController@index');
Route::get('/questions/{question}', 'QuestionsController@show');

// Post
Route::post('/questions', 'QuestionsController@store');
Route::post('/questions/{id}/answers', 'AnswerController@store');

// Fallback
Route::fallback(function () {
    abort(404, 'Page doesn\'t exist');
});