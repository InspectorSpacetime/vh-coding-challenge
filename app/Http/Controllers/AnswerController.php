<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;

class AnswerController extends Controller
{
    /**
     * Insert an Answer into the database.
     *
     * @return Redirect
     */
    public function store() {
        // Validate the input.
        request()->validate([
            'answerText' => [
                'required',
                'min:5'
            ],
            'questionId' => [
                'required'
            ]
        ]);

        // Commit the answer to the database
        $answer = new Answer();
        $answer->text = request('answerText');
        $answer->question_id = request('questionId');
        $answer->save();

        // Redirect to the question page we created an answer for.
        return redirect('/questions/' . $answer->question_id);
    }
}
