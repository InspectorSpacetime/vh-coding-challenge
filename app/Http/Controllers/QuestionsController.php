<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Question;
use App\PlaceholderQuestion;

class QuestionsController extends Controller
{
    /**
     * Show the home page, all questions and placeholder question text.
     *
     * @return View
     */
    public function index() {
        return view('home', [
            'placeholderQuestion' => PlaceholderQuestion::inRandomOrder()->firstOrFail(),
            'questions' => Question::latest()->with('answers')->get()
        ]);
    }

    /**
     * Show the full question and its comments.
     *
     * @param  Question  $question
     * @return View
     */
    public function show(Question $question)
    {
        // Retrieve the question from the database or show a 404 page if it doesn't exist.
        return view('question',['question' => $question]);
    }

    /**
     * Insert a Question into the database.
     *
     * @return Redirect
     */
    public function store() {
        // Validate the input. We use a closure to validate for the ? suffix instead of a Laravel Rule as we only use it in this one place.
        request()->validate([
            'questionText' => [
                'required',
                'min:5',
                function ($attribute, $value, $fail) {
                    // If the last character isn't a question mark then the question isn't valid.
                    if ($value[strlen($value)-1] != '?') {
                        $fail('Questions must end with ?');
                    }
                }
            ]
        ]);

        // Commit the question to the database
        $question = new Question();
        $question->text = request('questionText');
        $question->save();

        // Redirect to the new question page.
        return redirect('/questions/' . $question->id);
    }
}
