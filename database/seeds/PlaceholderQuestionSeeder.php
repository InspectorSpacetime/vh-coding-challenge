<?php

use Illuminate\Database\Seeder;
use App\PlaceholderQuestion;

class PlaceholderQuestionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PlaceholderQuestion::insert([
            'text' => 'How do cows make milk?'
        ]);
        PlaceholderQuestion::insert([
            'text' => 'How can I make vegan cheese at home?'
        ]);
        PlaceholderQuestion::insert([
            'text' => 'Where can I buy vegan suit shoes?'
        ]);
        PlaceholderQuestion::insert([
            'text' => 'What is your favourite vegan food?'
        ]);
        PlaceholderQuestion::insert([
            'text' => 'Why do humans kill animals?'
        ]);
    }
}
