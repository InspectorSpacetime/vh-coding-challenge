@extends ('layout')

@section ('content')
<!-- Question form -->
<div class="row">
    <div class="col-lg-6 offset-lg-3 mb-5">
        <form method="POST" action="/questions">
            @csrf
            <div class="input-group">
                <input type="text" class="form-control @error('questionText') is-invalid @enderror" name="questionText" id="questionText" placeholder="{{ $placeholderQuestion->text }}" value="{{ old('questionText') }}" >
                <div class="input-group-append">
                    <button class="btn btn-outline-primary" type="submit">Post</button>
                </div>
            </div>
            @error('questionText')
                <small class="form-text validation-error-message">
                    {{ $errors->first('questionText') }}
                </small>
            @enderror
        </form>
    </div>
</div>

<!-- Existing questions -->
<div class="row">
    <div class="col-lg-6 offset-lg-3">
        <h4>Existing Questions</h4>
        <ul class="list-group">
            @foreach ($questions as $question)
            <a href="/questions/{{ $question->id }}"><li class="list-group-item d-flex justify-content-between align-items-center">
                {{ $question->text }}
                <span class="badge badge-pill @if ($question->answers()->count() === 0) badge-warning @else badge-success @endif">{{ $question->answers()->count() }} answers</span>
            </li></a>
            @endforeach
            <!-- badge-warning for 0 or badge-success for >1 answers -->
        </ul>
    </div>
</div>
@endsection