@extends ('layout')

@section ('content')
<!-- Question text -->
<div class="row mb-2">
    <div class="col-lg-6 offset-lg-3">
        <h4>{{ $question->text }}</h4>
    </div>
</div>

<!-- Existing answers -->
<div class="row">
    <div class="col-lg-6 offset-lg-3">
        <ul class="list-group list-group-flush">
            @forelse ($question->answers as $answer)
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    {{ $answer->text }}
                </li>
            @empty
                <li class="list-group-item d-flex justify-content-between align-items-center">
                    <small class="form-text validation-error-message">No answers yet.</small>
                </li>
            @endforelse
        </ul>
    </div>
</div>

<!-- Add answer form -->
<div class="row mt-5">
    <div class="col-lg-6 offset-lg-3">
        <form method="POST" action="/questions/{{ $question->id }}/answers">
            @csrf
            <input type="hidden" name="questionId" id="questionId" value="{{ $question->id }}" />
            <div class="input-group">
                <textarea name="answerText" id="answerText" class="form-control @error('answerText') is-invalid @enderror">{{ old('answerText') }}</textarea>
                <div class="input-group-append">
                <button class="btn btn-outline-primary" type="submit">Post</button>
                </div>
            </div>
            @error('answerText')
                <small class="form-text validation-error-message">
                    {{ $errors->first('answerText') }}
                </small>
            @enderror
        </form>
    </div>
</div>
@endsection